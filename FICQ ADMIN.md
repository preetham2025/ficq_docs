FICQ WEB EDITING DOCUMENTATION
===================
Version:0.0.2
Author : Preetham 
email: preetham2025@gmail.com

TABLE OF CONTENTS
-------------

[TOC]


----------
Introduction
-------------
Ficq website is built using Worpress CMS. Almost everything in the website is editable via the Wordpress Dashboard without any coding. This documentation will guide you to administrate/moderate FICQ website.

You can learn more about wordpress here https://wordpress.org/about/.


Dashboard( Admin Area)
-------------
To access the Dashboard visit http://ficq.org.au/wp-admin/ and enter the **Username** and **Password** .

![enter image description here](http://puu.sh/pRX2I/863320abd1.png)

You will be redirected to the **Dashboard** Area. Take few minutes to familiarize with word press dashboard. Browse through the menu at the left side.

![enter image description here](http://puu.sh/pRXcd/9b0a640a82.png)


#### Add Administrator/Moderator

Go to  **User** > **Add New**  and enter the required information, change the **role** to administer or moderator based on your requirement. An email will be sent to the new user with instructions to access the account. 
> **Note:**

> - Please be careful when sharing password to the administrator account.
> - Always limit the number of administrator to 3.
> - Follow the guide to learn more about user roles, http://www.wpbeginner.com/beginners-guide/wordpress-user-roles-and-permissions/

#### Managing Users

Go to **User** > **All Users**, you can view all the current users and perform actions such as Delete.
>Do not delete any administrator account which is not created by you.

#### Plugins

Do not add any plugins, if you need any functionality please contact the developer.


----------

Add Posts (News) / Pages
-------------------
#### <i class="icon-file"></i> Creating new post

Go to **Posts** > **Add New**

Add the title and content text area of the post. You can copy past raw data into the content, it will automatically generate html. If you wish to change the html click on Text tab at top right corner of the content text area.Click Publish.
#### <i class="icon-file"></i> Always set Featured image for every post
Please remember to set the featured image for the post which is located on sidebar panel.
![enter image description here](http://puu.sh/pRYLr/c2ab0fc70a.png)

The featured image has to be set, as it will appear as a thumbnail in the home page.

#### Sharing post on social media

Once the post is published it can be shared to Twitter of Facebook page by clicking the share button at the bottom of the post.

#### Important Note !

Do not add more than **2** images per post as the site gets bigger, the bandwidth usage will increase and wordpress does not handle many images well.
Alternatively you can use Flickr app to add more photos, please check the Flickr Image Gallery section to understand flickr plugin.

----------

Add Events 
-------------------
#### <i class="icon-file"></i> Creating new event

Go to Events > Add new and fill in the details of the event.Click Publish.

![enter image description here](http://puu.sh/pRZpx/4fd70b3e1d.png)

#### <i class="icon-file"></i> Always set Featured image for every Event
Please remember to set the featured image for the event which is located on sidebar panel.
![enter image description here](http://puu.sh/pRYLr/c2ab0fc70a.png)

The featured image has to be set, as it will appear as a thumbnail in the home page.

----------
Add/Edit Home Page slider 
-------------------
Go to Meta Slider > Meta slider.

![enter image description here](http://puu.sh/pRZAi/32c79ad9f6.png)

Click on Add slides to add new slides. You can add captions to each slider.
For more information visit https://www.metaslider.com/documentation/getting-started-with-meta-slider/
> **Note:**
> Add images which are 500kb~ 1mb or lesser in size

----------
Add/Edit Sponsor logo slider
-------------------
Go to **Logo Showcase** > **Add new**, choose the category as Logo-main and the link to sponsors website if available.  Set the Featured image with logo image.

![enter image description here](http://puu.sh/pRZUR/28bc354e2f.png)

>Remember to select the category as Logo-main, otherwise the logo will not be displayed in the front page.

----------
Flickr Image Gallery
-------------------
Flickr is image/video storing website. Flickr has a file limit of 1tb per user which makes it convenient to store all your photos under one hosting.
The Flickr app has been integrated with FICQ website, all you need to do is login to Flickr and upload photos. Always remember to make your photos public.

To access and learn more about the Flickr plugin in Wordpress, login to wordpress and go to Setting>Flickr Justified Gallery. 

![enter image description here](http://puu.sh/pVjxE/94f2141c58.png)

This is the help section which guides you on how to add images/albums on any page/posts. The easiest way is to just use the following shortcode :

> [flickr_photostream]    

This displays all the images on your Flickr account. Please read through the section before using shortcodes.
> A shortcode is a WordPress-specific code that lets you do nifty things with very little effort. Shortcodes can embed files or create objects that would normally require lots of complicated, ugly code in just one line. Shortcode = shortcut. 


----------


MailChimp
-------------------

Mail chimp is integrated with FICQ, users can subscribe your news letter.
![enter image description here](http://puu.sh/pVkep/76962208b7.png)

You can login to the given Mailchimp account to send news letter to your subscribed users.
Follow the guide below to send newsletter. 
http://kb.mailchimp.com/getting-started/getting-started-with-mailchimp

> Note: Subscribers List has already been created.

----------

Google Analytics 
-------------------

Google Analytics is already integrated to FICQ website, it is available at the dashboard page .

![enter image description here](http://puu.sh/pVk2E/40eaa25ffe.png)

Further more you can access google analytics via your google account.

----------


Google Drive
-------------------

All official documentations are stored in google drive. If you would like to add documents, you can access your google drive account.

If you would like to add folders on the page/post you can use the following code.

List view
> < iframe src="https://drive.google.com/embeddedfolderview?id=YOURID#list" width="700" height="500" frameborder="0"></iframe>


This will show a list of files and folders within the folder you selected. To show a grid view (preview thumbnails of the files and folders), you simply alter the URL and change #list with #grid.

Grid view

> < iframe src="https://drive.google.com/embeddedfolderview?id=YOURID#grid" width="700" height="500" frameborder="0"></iframe>

Just make sure the folder is accessible to view, so check your folder permission within Google Drive.

----------